#! -*- coding: utf-8 -*-
import os
import pickle
import argparse
from utils import *
import pandas as pd
from nltk.tokenize import word_tokenize
from keras.callbacks import CSVLogger, ModelCheckpoint
from keras.preprocessing.sequence import pad_sequences

VALIDATION_SPLIT = 0.2
BATCH_SIZE = 256
EPOCHS = 10
VERBOCITY = 1

if __name__ == '__main__':
    predicting_classes = ["toxic", "severe_toxic", "obscene", "threat", "insult", "identity_hate"]
    arg_parser = argparse.ArgumentParser('This script is used to train, evaluate and save models')
    arg_parser.add_argument('--eval', action='store_true')
    arg_parser.add_argument('--epochs', type=int, help='Epochs count to train', default=10)
    arg_parser.add_argument('--bsize', type=int, help='Batch size', default=256)
    arg_parser.add_argument('--data', type=str, help='Path to dataset')
    arg_parser.add_argument('--train_cf', type=str, help='Train configuration file')
    arg_parser.add_argument('--maxlen', type=int, default=200)
    arg_parser.add_argument('--vocab_out', type=str, default=None, help='Path to save vocabulary')
    args = arg_parser.parse_args()

    train_dataset = pd.read_csv(args.data, encoding='utf-8')
    y_train = train_dataset[predicting_classes].values
    train_texts = train_dataset.comment_text.str.lower()
    train_texts = train_texts.apply(word_tokenize)

    eval_texts = []
    y_eval = []
    if args.eval:
        split_idx = int(VALIDATION_SPLIT*train_texts.shape[0])
        eval_texts = train_texts[-split_idx:]
        train_texts = train_texts[:-split_idx]
        y_eval = y_train[-split_idx:]
        y_train = y_train[:-split_idx]

    X_train, vocab = maps_to_ids(train_texts.values)
    X_eval, vocab = maps_to_ids(eval_texts, vocab=vocab, update=False)
    X_train = pad_sequences(X_train, maxlen=args.maxlen, truncating='post', padding='post', value=0)
    X_eval = pad_sequences(X_eval, maxlen=args.maxlen, truncating='post', padding='post', value=0)

    model = model_from_config_file(args.train_cf, vocab_size=len(vocab), input_maxlen=args.maxlen)
    model_spec = os.path.basename(args.train_cf).strip('yml') + '{}_{}'.format(len(vocab), args.maxlen)
    logfile = 'logs/' + model_spec + '.csv'
    model_save_path = 'trained_models/' + model_spec + '_{epoch:02d}.hdf5'

    model.fit(
        X_train, y_train,
        epochs=args.epochs,
        batch_size=args.bsize,
        verbose=VERBOCITY,
        validation_data=(X_eval, y_eval) if args.eval else None,
        callbacks=[CSVLogger(logfile), ModelCheckpoint(model_save_path)]
    )

    if args.vocab_out is not None:
        with codecs.open(args.vocab_out, 'wb') as output_stream:
            pickle.dump(vocab, output_stream)
