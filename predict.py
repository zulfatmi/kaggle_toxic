from keras.preprocessing.sequence import pad_sequences
from nltk.tokenize import word_tokenize
from keras.models import load_model
from utils import maps_to_ids
import pandas as pd
import argparse
import pickle
import codecs


if __name__ == '__main__':

    predicting_classes = ["toxic", "severe_toxic", "obscene", "threat", "insult", "identity_hate"]
    arg_parser = argparse.ArgumentParser()
    arg_parser.add_argument('--model', type=str, help='Path to the trained model')
    arg_parser.add_argument('--data', type=str, help='Path to the data')
    arg_parser.add_argument('--out', type=str, default=None, help='Output file')
    arg_parser.add_argument('--vocab', dtype=str, default='vocabs/vocab.bin', help='Path to vocabulary file')
    arg_parser.add_argument('--charlvl', action='store_true')
    args = arg_parser.parse_args()

    model = load_model(args.model)
    with codecs.open(args.vocab, 'rb') as input_steam:
        vocab = pickle.load(input_steam)

    dataset = pd.read_csv(args.data)
    text = dataset.comment_text.str.lower()
    text = text.apply(word_tokenize).values
    text = maps_to_ids(text, vocab=vocab, update=False)
    text = pad_sequences(text, 200, truncating='post', padding='post', value=0)

    y_pred = model.predict(text)
    y_pred = pd.DataFrame(y_pred, columns=predicting_classes)
    if args.out is not None:
        pd.concat([dataset, y_pred]).to_csv(args.out)
