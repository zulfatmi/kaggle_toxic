#! -*- coding: utf-8 -*-
from keras.layers import Conv1D, Input, \
    Embedding, Dense, concatenate, GlobalMaxPool1D, LSTM, GRU
from keras.layers.wrappers import Bidirectional
from keras.models import Model
import numpy as np
import keras.backend as K
import tensorflow as tf


def precision_on(n):

    def precision(y_true, y_pred):
        score, up_opt = tf.metrics.precision(y_true[:, n], y_pred[:, n])
        K.get_session().run(tf.local_variables_initializer())
        with tf.control_dependencies([up_opt]):
            score = tf.identity(score)
        return score

    return precision


def recall_on(n):

    def recall(y_true, y_pred):
        score, up_opt = tf.metrics.recall(y_true[:, n], y_pred[:, n])
        K.get_session().run(tf.local_variables_initializer())
        with tf.control_dependencies([up_opt]):
            score = tf.identity(score)
        return score

    return recall


def conv_model(input_maxlen,
               vocab_size,
               cwindow_sizes=[3, 5, 7],
               fcounts=[32, 32, 32]):

    inputs = Input(shape=(input_maxlen,), dtype='int32')

    embedding_layer = Embedding(vocab_size, 300, input_length=input_maxlen)(inputs)

    conv_layers = []
    for cwindow_size, fcount in zip(cwindow_sizes, fcounts):
        conv_layer = Conv1D(fcount, cwindow_size, padding='same')(embedding_layer)
        mpooled_conv_layer = GlobalMaxPool1D()(conv_layer)
        conv_layers.append(mpooled_conv_layer)

    conv_outs = concatenate(conv_layers, axis=-1)

    dense_layer = Dense(6, activation='sigmoid')(conv_outs)

    model = Model(inputs, dense_layer)

    model.compile(
        optimizer='adam',
        loss='binary_crossentropy',
        metrics=['accuracy'] + [precision_on(i) for i in range(6)] + [recall_on(i) for i in range(6)]
    )
    return model


def bilstm_model(input_maxlen, vocab_size, rnn_size):

    inputs = Input(shape=(input_maxlen,), dtype='int32')
    embeddings_layer = Embedding(vocab_size, 300, input_length=input_maxlen)(inputs)

    rnn_layer = Bidirectional(LSTM(rnn_size, return_sequences=False))(embeddings_layer)

    dense_layer = Dense(6, activation='sigmoid')(rnn_layer)

    model = Model(inputs, dense_layer)

    model.compile(
        optimizer='adam',
        loss='binary_crossentropy',
        metrics=['accuracy'] + [precision_on(i) for i in range(6)] + [recall_on(i) for i in range(6)]
    )

    return model


def bigru_model(input_maxlen, vocab_size, rnn_size):

    inputs = Input(shape=(input_maxlen,), dtype='int32')
    embeddings_layer = Embedding(vocab_size, 300, input_length=input_maxlen)(inputs)

    rnn_layer = Bidirectional(GRU(rnn_size, return_sequences=False))(embeddings_layer)

    dense_layer = Dense(6, activation='sigmoid')(rnn_layer)

    model = Model(inputs, dense_layer)

    model.compile(
        optimizer='adam',
        loss='binary_crossentropy',
        metrics=['accuracy'] + [precision_on(i) for i in range(6)] + [recall_on(i) for i in range(6)]
    )

    return model

Models = {
    'convolutional_model': conv_model,
    'bidir_lstm': bilstm_model,
    'bidir_gru': bigru_model
}


if __name__ == '__main__':
    """
        Тестирование моделей
    """
    smaxlen = 10
    vocab_size = 1000
    ex_count = 10000
    classes_cnt = 10
    model = conv_model(smaxlen, vocab_size)
    X = np.random.randint(0, vocab_size, (ex_count, smaxlen))
    y = np.random.randint(0, 2, (ex_count, classes_cnt))
    model.fit(X, y, epochs=10, batch_size=256, verbose=1, validation_split=0.2)
    y_pred = model.predict(X[:10])
    y_pred = np.round(y_pred)
