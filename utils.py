import re
import yaml
import codecs
import numpy as np
from models import Models
from tqdm import tqdm


def maps_to_ids(sequences, vocab=None, update=True):
    if vocab is None: vocab = {'<PD_VAL>': 0, '<UNKNOWN>': 1}
    mapped_sequences = []
    for sequence in sequences:
        mapped_sequence = []
        for token in sequence:
            if token not in vocab:
                if update: vocab[token] = len(vocab)
                else: token = '<UNKNOWN>'
            mapped_sequence.append(vocab[token])
        mapped_sequences.append(mapped_sequence)

    return mapped_sequences, vocab


def split_by_unds(tokens):
    splitted = map(lambda token: token.split('_'), tokens)
    return sum(splitted, [])


def replace_non_char_or_numeric(tokens):

    for i in range(len(tokens)):
        token = tokens[i]
        if len(token) >= 5 and re.match(r'[a-zA-Z0-9]', token):
            tokens[i] = '<LNONCORN>'

    return tokens


def replace_nums(tokens):
    for i in range(len(tokens)):
        token = tokens[i]
        if token.isdigit():
            tokens[i] = '<NUM>'
    return tokens


def ngram_generator(sequence, ngram_range=range(1, 3)):
    sequence_length = len(sequence)
    for ngram_length in ngram_range:
        for start_index in range(sequence_length - ngram_length + 1):
            yield ' '.join(sequence[start_index:start_index+ngram_length])


def calculate_pmi(texts, labels, vocab):
    coocurence_matrix = np.zeros((len(vocab), len(labels[0])))
    word_occurences = np.zeros(len(vocab))
    label_occurences = np.zeros(len(labels[0]))
    for text, ex_labels in tqdm(zip(texts, labels)):
        for token in text:
            if token not in vocab: continue
            token_idx = vocab[token]
            word_occurences[token_idx] += 1
            for label_idx in np.where(ex_labels==1):
                label_occurences[label_idx] += 1
                coocurence_matrix[token_idx, label_idx] += 1

    pmi_index = coocurence_matrix/word_occurences.reshape(-1, 1)/label_occurences
    return np.log(pmi_index+1)


def load_params(path):
    with codecs.open(path) as input_stream:
        return yaml.load(input_stream)


def model_from_config_file(path, **kwargs):
    config = load_params(path)
    model = Models[config['name']]
    kwargs.update(config['model_params'])
    return model(**kwargs)


if __name__ == '__main__':
    model_from_config_file('configs/baseline_model.yml', vocab_size=100, input_maxlen=200)